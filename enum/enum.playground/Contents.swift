import UIKit
//Создайте по 2 enum с разным типом RawValue
enum Suit : String {
    case Diamonds = "♦️"
    case Clubs = "♣️"
    case Hearts = "♥️"
    case Spades = "♠️"
}
enum Value : Int{
    case Two = 2
    case Three = 3
    case Four = 4
    case Five = 5
    case Six = 6
    case Seven = 7
    case Eight = 8
    case Nine = 9
    case Ten = 10
    case Jack = 11
    case Queen = 12
    case King = 13
    case Ace = 1
}



//Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж
enum Sex{
    case Male
    case Female
}

enum YearsOld{
    case young
    case middleAge
    case old
}
enum Experience{
    case low
    case mid
    case high
}


//Создать enum со всеми цветами радуги
//Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль. Пример результата в консоли 'apple green', 'sun red' и т.д.
func coloredItems(){
    enum ColorRainbow : String{
        case Red = "red"
        case Orange = "orange"
        case Yellow = "yellow"
        case Green = "green"
        case Blue = "blue"
        case Indigo = "indigo"
        case Violet = "violet"
    }
    enum Items : String{
        case apple = "Apple"
        case banana = "Banana"
        case sun = "Sun"
        case cucumber = "Cucumber"
        case sky = "Sky"
    }
    var someItems : [Items :ColorRainbow] = [:]
    someItems[.apple] = .Red
    someItems[.banana] = .Yellow
    someItems[.sky] = .Blue
    someItems[.sun] = .Red
    someItems[.cucumber] = .Green
    for (key, value) in someItems{
        print("The \(key.rawValue) has a \(value.rawValue) color")
    }
}
coloredItems()
print("\n\n\n")




//Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы} и выводит числовое значение оценки
enum Grades : String{
    case A = "Excellent"
    case B = "Good"
    case C = "Satisfactory"
    case D = "Non-satisfactory"
    case F = "Non-satisfactory and vary bad"
}



func goodTeacher(names : [String], rand : () -> (Grades))
{
    
    for i in names{
        let grade : Grades = rand()
        print("\(i) got rate is \(grade), that is \(grade.rawValue)")
    }
}

var names : [String] = ["Liam", "Olivia", "Noa", "Emma", "Oliver","Charlotte"]

goodTeacher(names: names, rand: {() -> Grades in
    let i = Int.random(in: 1...5)
    switch i{
    case 5 : return .A
    case 4 : return .B
    case 3 : return .C
    case 2 : return .D
    case 1 : return .F
    default:
        return .F
    }
})
print("\n\n\n")
//Создать метод, которая выводит в консоль какие автомобили стоят в гараже, используйте enum


enum Cars{
    case Porshe
    case Mazda
    case Chevrolet
    case Hyundai
    case Wolksvagen
}

var cars : [Int : Cars] = [1 : .Chevrolet,
                             2 : .Hyundai,
                             3 : .Mazda,
                             4 : .Wolksvagen,
                             5 : .Porshe]

func printGarage(_ cars : [Int : Cars]){
    for i in cars{
        print(i.key, i.value)
    }
}

printGarage(cars)
